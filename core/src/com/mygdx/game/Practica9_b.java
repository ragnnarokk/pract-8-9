package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by joan.tuset on 08/03/2017.
 */

public class Practica9_b extends ApplicationAdapter implements InputProcessor {
    SpriteBatch batch;
    ParticleEffect flame;

    @Override
    public void create () {
        batch = new SpriteBatch();
        flame = new ParticleEffect();
        Gdx.input.setInputProcessor((InputProcessor) this);
        flame.load(Gdx.files.internal("Flame.part"),Gdx.files.internal(""));
        flame.setPosition(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
        flame.start();
    }

    @Override
    public void render () {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        flame.update(Gdx.graphics.getDeltaTime());

        batch.begin();
        flame.draw(batch);
        batch.end();

        if(flame.isComplete()){flame.reset();}
    }

    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.NUM_1){
            flame.scaleEffect(0.5f);
        }
        if(keycode == Input.Keys.NUM_2){
            flame.scaleEffect(1);
        }
        if(keycode == Input.Keys.NUM_3){
            flame.scaleEffect(1.5f);
        }
        if(keycode == Input.Keys.NUM_4){
            flame.scaleEffect(2);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        flame.setPosition(screenX,Gdx.graphics.getHeight() - screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void dispose () {

    }
}
